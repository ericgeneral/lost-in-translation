# Lost in Translation
This website translates English text into American Sign Language fingerspelling. It was created with React, Redux, and json-server. When logging in for the first time, it takes heroku/json-server several seconds to get warmed up.

## URL
https://eric-general-translate.herokuapp.com/

export const ACTION_LOGOUT_ATTEMPTING = '[logout] ATTEMPT';
export const ACTION_LOGOUT_SUCCESS = '[logout] SUCCESS';
export const ACTION_LOGOUT_ERROR = '[logout] ERROR';

export const logoutAttemptAction = (profile) => ({
  type: ACTION_LOGOUT_ATTEMPTING,
  payload: profile
});

export const logoutSuccessAction = () => ({
  type: ACTION_LOGOUT_SUCCESS
});

export const logoutErrorAction = (error) => ({
  type: ACTION_LOGOUT_ERROR,
  payload: error
});

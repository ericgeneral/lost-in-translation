import { applyMiddleware } from 'redux';
import { loginMiddleware } from './loginMiddleware';
import { logoutMiddleware } from './logoutMiddleware';
import { sessionMiddleware } from './sessionMiddleware';

export default applyMiddleware(
  loginMiddleware,
  sessionMiddleware,
  logoutMiddleware
);
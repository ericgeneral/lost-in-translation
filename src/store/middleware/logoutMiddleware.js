import { LogoutAPI } from "../../components/Heading/LogoutAPI";
import { DatabaseAPI } from "../../Database/DatabaseAPI";
import { ACTION_LOGOUT_ATTEMPTING, ACTION_LOGOUT_SUCCESS, logoutErrorAction, logoutSuccessAction } from "../actions/logoutAction";
import { sessionClearAction } from "../actions/sessionAction";

const clearUserHistoryInDatabase = (dispatch, profile) => {
  DatabaseAPI.deleteHistory(profile)
    .then((results) => {
      dispatch(logoutSuccessAction());
    })
    .catch(error => dispatch(logoutErrorAction(error)));
}

export const logoutMiddleware = ({ dispatch }) => next => action => {
  next(action);

  if (action.type === ACTION_LOGOUT_ATTEMPTING) {
    LogoutAPI.logout(action.payload)
      .then(clearUserHistoryInDatabase(dispatch, action.payload))
      .catch(error => dispatch(logoutErrorAction(error)));
  }

  if (action.type === ACTION_LOGOUT_SUCCESS) {
    dispatch(sessionClearAction());
  }
};

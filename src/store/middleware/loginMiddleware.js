import { LoginAPI } from "../../components/Login/LoginAPI";
import { DatabaseAPI } from "../../Database/DatabaseAPI";
import { ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_SUCCESS, loginErrorAction, loginSuccessAction } from "../actions/loginActions";
import { sessionSetAction } from "../actions/sessionAction";

const checkForUserInDatabase = (dispatch, profile) => {
  DatabaseAPI.getUser(profile.username)
    .then(existingUser => {
      if (existingUser.length === 0) {
        addUserToDatabase(dispatch, profile);
      } else {
        dispatch(loginSuccessAction(existingUser[0]));
      }
    })
    .catch(error => dispatch(loginErrorAction(error)));
}

const addUserToDatabase = (dispatch, profile) => {
  DatabaseAPI.addUser(profile)
    .then(newUser => {
      dispatch(loginSuccessAction(newUser));
    })
    .catch(error => dispatch(loginErrorAction(error)));
}

export const loginMiddleware = ({ dispatch }) => next => action => {
  next(action);

  if (action.type === ACTION_LOGIN_ATTEMPTING) {
    LoginAPI.login(action.payload)
      .then(profile => checkForUserInDatabase(dispatch, profile))
      .catch(error => dispatch(loginErrorAction(error)));
  }

  if (action.type === ACTION_LOGIN_SUCCESS) {
    dispatch(sessionSetAction(action.payload));
  }
}
import { cookies } from "../../cookies/cookies";
import { DatabaseAPI } from "../../Database/DatabaseAPI";
import { ACTION_SESSION_CLEAR, ACTION_SESSION_INIT, ACTION_SESSION_SET, sessionClearAction, sessionSetAction } from "../actions/sessionAction";

export const sessionMiddleware = ({ dispatch }) => next => action => {
  next(action);

  if (action.type === ACTION_SESSION_INIT) {
    const session = cookies.getCookie();
    if (session === null || session === '')
      return;
    DatabaseAPI.getUser(session)
      .then(existingProfile => {
        dispatch(sessionSetAction(...existingProfile));
      })
      .catch(error => {
        console.log("Couldn't retrieve history from the database. Error: " + error.message);
        dispatch(sessionClearAction());
      })
  }

  if (action.type === ACTION_SESSION_SET) {
    cookies.setCookie(action.payload);
  }

  if (action.type === ACTION_SESSION_CLEAR) {
    cookies.deleteCookie();
  }
};

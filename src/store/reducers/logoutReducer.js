import { ACTION_LOGOUT_ATTEMPTING, ACTION_LOGOUT_ERROR, ACTION_LOGOUT_SUCCESS } from "../actions/logoutAction"

const initialState = {
  logoutAttempting: false,
  logoutError: ''
}

export const logoutReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_LOGOUT_ATTEMPTING:
      return {
        ...state,
        logoutAttempting: true,
        logoutError: ''
      };
    case ACTION_LOGOUT_SUCCESS:
      return {
        ...initialState
      };
    case ACTION_LOGOUT_ERROR:
      return {
        ...state,
        logoutAttempting: false,
        logoutError: action.payload.message
      }
    default:
      return state;
  }
};

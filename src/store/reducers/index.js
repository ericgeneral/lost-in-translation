import { combineReducers } from 'redux';
import { loginReducer } from './loginReducer';
import { sessionReducer } from './sessionReducer';
import { logoutReducer } from './logoutReducer';

const appReducer = combineReducers({
  loginReducer,
  sessionReducer,
  logoutReducer
});

export default appReducer;
import { ACTION_SESSION_CLEAR, ACTION_SESSION_SET } from "../actions/sessionAction";

const initialState = {
  profile: {
    username: '',
    id: 0,
    history: []
  },
  loggedIn: false
};

export const sessionReducer = (state = initialState, action) => {
  switch(action.type) {
    case ACTION_SESSION_SET:
      return {
        ...state,
        profile: action.payload,
        loggedIn: true
      }
    case ACTION_SESSION_CLEAR:
      return {
        ...initialState
      }
    default:
      return state;
  }
};

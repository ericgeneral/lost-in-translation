import styles from "./Heading.module.css";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { logoutAttemptAction } from "../../store/actions/logoutAction";

const Heading = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { profile, loggedIn } = useSelector(state => state.sessionReducer);

  const onTranslateClicked = () => {
    if (history.location.pathname === "/translate") {
      return;
    }
    history.push("/translate");
  }

  const onProfileClicked = () => {
    if (history.location.pathname === "/profile") {
      return;
    }
    history.push("/profile");
  }

  const onLogoutClicked = () => {
    dispatch(logoutAttemptAction(profile));
  }

  return (
    <div className={ "mb-3 " + styles.Heading }>
      <h1 className={ styles.HeadingText }>Lost in Translation</h1>
      { loggedIn &&
        <div className="dropdown show">
          <button className="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div className={ styles.UserGrid }>
              <span className={ "material-icons-outlined " + styles.UserGridItem }>
                account_circle
            </span>
              <h3 className={ styles.UserGridItem }>{ profile.username }</h3>
            </div>
          </button>
          <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <div className="dropdown-item" onClick={ onTranslateClicked }>Translate</div>
            <div className="dropdown-item" onClick={ onProfileClicked }>Profile</div>
            <div className="dropdown-item" onClick={ onLogoutClicked }>Logout</div>
          </div>
        </div>
      }
    </div>
  );
};

export default Heading;

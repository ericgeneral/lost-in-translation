import AppContainer from "../../hoc/AppContainer";
import styles from "./Translate.module.css";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router";
import { filterText } from "../../util/common";
import { DatabaseAPI } from "../../Database/DatabaseAPI";
import { sessionSetAction } from "../../store/actions/sessionAction";

const Translate = () => {
  const { profile, loggedIn } = useSelector(state => state.sessionReducer);
  const { logoutError } = useSelector(state => state.logoutReducer);
  const dispatch = useDispatch();

  const [state, setState] = useState({
    text: '',
    changed: false,
    filteredText: []
  });

  const [error, setError] = useState(null);

  const onTextChanged = (event) => {
    setState({
      ...state,
      changed: true,
      text: event.target.value
    });
    setError(null);
  }

  const onFormSubmit = (event) => {
    event.preventDefault();
    if (!state.changed || state.text.trim() === '') {
      return;
    }

    updateDatabaseAndSession();
    
    setState({
      ...state,
      changed: false,
      filteredText: filterText(state.text)
    });
  }

  const updateDatabaseAndSession = () => {
    let updatedHistory = [];
    if (profile.history === undefined || profile.history === null)
      updatedHistory = [state.text]
    else
      updatedHistory = profile.history.concat(state.text);
    
    DatabaseAPI.updateHistory(profile, updatedHistory)
      .then(() => {
        dispatch(sessionSetAction({
          ...profile,
          history: updatedHistory
        }));
      })
      .catch(error => setError('Error saving transation to history. ' + error.message));
  }

  return (
    <>
      {
        !loggedIn &&
        <Redirect to="/" />
      }
      {
        loggedIn &&
        <AppContainer>
          <div className={ styles.TextInput }>
            <form onSubmit={ onFormSubmit }>
              <div className="input-group">
                <div className="input-group-prepend">
                    <span className="input-group-text material-icons-outlined">
                      keyboard
                    </span>
                </div>
                <input id="textToTranslate" 
                  className="form-control" 
                  type="text" placeholder="Enter text to translate here." 
                  aria-label="textToTranslate"
                  maxLength="40" 
                  onChange={ onTextChanged }/>
                <div className="input-group-append">
                  <button type="submit" className="btn btn-light input-group-text material-icons-outlined">
                      login
                  </button>
                </div>
              </div>
            </form>
          </div>
          <div className={ styles.TranslateOuter }>
            <div className={ styles.TranslateInner }>
              { state.filteredText.map((character, index) => { 
                  return <img className={ styles.SignedImage } src={ process.env.PUBLIC_URL + "/assets/images/individual_signs/" + character + ".png" } alt={ character } key={"translation" + index} />
                })
              }
            </div>
          </div>
          {
            error &&
            <p className={ styles.TranslateError }>{ error }</p>
          }
          {
            logoutError &&
            <p className={ styles.TranslateError }>Error logging out. { logoutError }</p>
          }
        </AppContainer>
      }
    </>
  );
};

export default Translate;

import { Link } from 'react-router-dom';
import AppContainer from '../../hoc/AppContainer';
import styles from './NotFound.module.css';

const NotFound = () => {
  return (
    <AppContainer>
      <div className={ styles.NotFoundLayout }>
        <img className={ styles.NotFoundLogo } 
             src={ process.env.PUBLIC_URL + "/assets/images/Logo.png" } 
             alt="Logo" />
        <div className={ styles.NotFoundDescription }>
          <h1>Oops!</h1>
          <p>The page you're looking wasn't found.</p>
          <Link to="/" className={ styles.link }>Home</Link>
        </div>
      </div>
    </AppContainer>
  );
};

export default NotFound;
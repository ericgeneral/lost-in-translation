import AppContainer from '../../hoc/AppContainer';
import { useState } from 'react';
import styles from './Login.module.css';
import { useDispatch, useSelector } from 'react-redux';
import { loginAttemptAction } from '../../store/actions/loginActions';
import { Redirect } from 'react-router';

const Login = () => {
  const dispatch = useDispatch();
  const { loginAttempting, loginError } = useSelector(state => state.loginReducer);
  const { loggedIn } = useSelector(state => state.sessionReducer);

  const [credentials, setCredentials] = useState({
    username: ''
  });

  const onFormSubmit = (event) => {
    event.preventDefault();
    if (credentials.username === '')
      return;

    dispatch(loginAttemptAction(credentials));
  }

  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      [event.target.id]: event.target.value
    })
  }

  return (
    <>
      {
        loggedIn &&
        <Redirect to="/translate" />
      }
      {
        !loggedIn &&
        <AppContainer>
          <div className={ styles.LoginLayout }>
            <img className={ styles.LoginLogo } src={ process.env.PUBLIC_URL + "/assets/images/Logo.png" } alt="Logo" />
            <div className={ styles.LoginDescription }>
              <h1 className={ styles.DescriptionHeader }>Lost in Translation</h1>
              <p className={ styles.DescriptionText }>This website translates English text into American Sign Language fingerspelling.</p>
            </div>
            <div className={ styles.LoginInput }>
              <form onSubmit={ onFormSubmit }>
                <div className="input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text material-icons-outlined">
                      keyboard
                    </span>
                  </div>
                  <input id="username"
                    className="form-control"
                    type="text"
                    placeholder="What's your name?"
                    aria-label="username"
                    onChange={ onInputChange } />
                  <div className="input-group-append">
                    <button type="submit" className="btn btn-light input-group-text material-icons-outlined">
                      login
                    </button>
                  </div>
                </div>
              </form>
            </div>
            {
              loginAttempting &&
              <p className={ styles.LoginMessage }>Trying to login...</p>
            }
            {
              loginError && 
              <p className={ styles.LoginError }>Error logging in. { loginError }</p>
            }
          </div>
        </AppContainer>
      }
    </>
  );
}

export default Login;
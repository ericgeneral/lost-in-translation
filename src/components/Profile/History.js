import styles from './History.module.css';

const History = (props) => {
  return (
    <div>
      <h5 className={ styles.HistoryText }>{ props.text }</h5>
      <div className={ styles.HistoryTranslation }>
        { props.translation &&
          props.translation.map((character, index) => { 
            return <img className={ styles.SignedImage } src={ process.env.PUBLIC_URL + "/assets/images/individual_signs/" + character + ".png" } alt={ character } key={"translation" + index} />
          })
        }
      </div>
    </div>
  )
}

export default History;
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import AppContainer from '../../hoc/AppContainer';
import styles from './Profile.module.css';
import History from './History';
import { useEffect, useState } from 'react';
import { filterText } from '../../util/common';
import { DatabaseAPI } from '../../Database/DatabaseAPI';
import { sessionSetAction } from '../../store/actions/sessionAction';

const Profile = () => {
  const { profile, loggedIn } = useSelector(state => state.sessionReducer);
  const { logoutError } = useSelector(state => state.logoutReducer);
  const [ state, setState ] = useState({
    originalTexts: [],
    translations: []
  });
  const [ error, setError ] = useState(null);
  const dispatch = useDispatch();
  const numberOfEntriesToShow = 10;

  useEffect(() => {
    if (profile.history === null || profile.history === undefined)
      return;

    const originalTexts = [], translations = [];
    let profileHistoryIndex = 0, entriesShownIndex = 0;
    if (profile.history.length > numberOfEntriesToShow) {
      profileHistoryIndex = profile.history.length - numberOfEntriesToShow;
    }
    for( ; profileHistoryIndex < profile.history.length; profileHistoryIndex++, entriesShownIndex++) {
      originalTexts[entriesShownIndex] = profile.history[profileHistoryIndex];
      translations[entriesShownIndex] = filterText(profile.history[profileHistoryIndex]);
    }

    setState({
      originalTexts: originalTexts,
      translations: translations
    });
    setError(null);
  }, [profile.history]);

  const onClearClicked = () => {
    DatabaseAPI.deleteHistory(profile)
      .then(() => 
        dispatch(sessionSetAction({
          ...profile,
          history: []
        }))
      )
      .catch(error => setError('Error clearing the history. ' + error.message));
  }

  return (
    <>
      { 
        !loggedIn &&
        <Redirect to="/" />
      }
      { 
        loggedIn &&
        <AppContainer>
          <div className={ styles.History }>
            <div className={ styles.HistoryHeaderLayout}>
              <h3 className={ styles.HistoryHeader}>Recent Translations</h3>
              <button className={ "button " + styles.HistoryClear } onClick={ onClearClicked }>Clear</button>
            </div>
            { state.originalTexts.map((text, index) => { 
              return <History className={ styles.history } 
                              text={ text } 
                              translation={ state.translations[index] } 
                              key={ 'history' + index } />
            })}
          </div>
          {
            error &&
            <p className={ styles.HistoryError }>{ error }</p>
          }
          {
            logoutError &&
            <p className={ styles.HistoryError }>Error logging out. { logoutError }</p>
          }
        </AppContainer>
      }
    </>
  )
};

export default Profile;

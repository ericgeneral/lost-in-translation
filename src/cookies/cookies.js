export const cookies = {
  setCookie(session) {
    var date = new Date();
    date.setTime(date.getTime() + (1 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + date.toUTCString();
    // Just store the username in the cookie for now until an actual session is created from a log in.
    document.cookie = "session=" + session.username + ";" + expires + ";path=/";
  },
  getCookie() {
    const allCookies = document.cookie.split('; ')
    const cookie = allCookies.find(row => row.startsWith('session='))
    if (cookie) {
      return cookie.split('=')[1];
    } 
    return '';
  },
  deleteCookie(cookieName) {
    var date = new Date('August 19, 1975 23:15:30');
    var expires = "expires=" + date.toUTCString();
    document.cookie = "session=;" + expires + ";path=/";
  }
};

const baseUrl = 'https://lost-in-translation-database.herokuapp.com/';

const callDatabase = (subdirectory, method, body) => {
  return fetch(baseUrl + subdirectory, {
    method: method,
    headers: {
      'Content-Type': 'application/json'
    },
    body: body ? JSON.stringify(body) : undefined
  }).then(response => {
    if (!response.ok) {
      throw new Error(response.status + ": " + response.statusText);
    }
    return response.json();
  });
}

export const DatabaseAPI = {
  getUser(username) {
    return callDatabase('users/?username=' + username, 'GET', null);
  },
  addUser(profile) {
    return callDatabase('users', 'POST', profile);
  },
  updateHistory(profile, updatedHistory) {
    return callDatabase('users/' + profile.id, 'PATCH', { history: updatedHistory });
  },
  deleteHistory(profile) {
    return callDatabase('users/' + profile.id, 'PATCH', { history: [] });
  }
}
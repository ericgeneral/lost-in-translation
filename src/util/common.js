export const filterText = (text) => {
  const filter = /[A-Za-z]/g;
  const lowerCase = text.toLowerCase();
  let filtered = lowerCase.match(filter);
  if (filtered === null) {
    filtered = []
  }
  return filtered;
};
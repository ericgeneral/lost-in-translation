import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
import Heading from './components/Heading/Heading';
import Login from './components/Login/Login';
import NotFound from './components/NotFound/NotFound';
import Profile from './components/Profile/Profile';
import Translate from './components/Translate/Translate';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Heading />
        <Switch>
          <Route path="/" exact component={ Login } />
          <Route path="/translate" component={ Translate } />
          <Route path="/profile" component={ Profile } />
          <Route path="*" component={ NotFound } />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
